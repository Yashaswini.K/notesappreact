import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import AcUnitOutlinedIcon from "@mui/icons-material/AcUnitOutlined";
import SendIcon from "@mui/icons-material/Send";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import { makeStyles } from "@material-ui/core";
import TextField from "@mui/material/TextField";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";

import Grid from "@mui/material/Grid";
// import { useHistory } from "react-router-dom";

function Register() {
    // const history = useHistory();
    const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [age, setAge] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress] = useState("");
  const [nameErr, setNameErr] = useState(false);
  const [emailErr, setEmailErr] = useState(false);
  const [phoneErr, setPhoneErr] = useState(false);
  const [ageErr, setAgeErr] = useState(false);
  const [passwordErr, setPasswordErr] = useState(false);
  const [addressErr, setAddressErr] = useState(false);

  const [category, setCategory] = useState("male");

  const onSubmit = (e) => {
    setNameErr(false);
    setEmailErr(false);
    if (name === "") {
        setNameErr(true);
    }
    if (email === "") {
        setEmailErr(true);
    }
    if (phone === "") {
        setPhoneErr(true);
    }
    if (age === "") {
        setAgeErr(true);
    }
    if (password === "") {
        setPasswordErr(true);
    }
    if (address === "") {
        setAddressErr(true);
    }
    e.preventDefault();
    if (name && email && phone && age && password && address) {
        console.log(name, email, phone,age,password,category, address);
        debugger
        let obj={username:name,emailId:email,pwd:password}
        localStorage.setItem("data",JSON.stringify(obj))
   
    }
  };
  return (
    <Container>
    <Typography
      noWrap
      color="secondary"
      varient="h1"
      gutterBottom
      style={{ fontSize: "25px" }}
    >
      <b>Register</b>
    </Typography>
    <form noValidate autoComplete="off" onSubmit={onSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setName(e.target.value)}
            label="UserName"
            variant="outlined"
            color="secondary"
            fullWidth
            required
            error={nameErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>
       
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setEmail(e.target.value)}
            label="Email"
            variant="outlined"
            color="secondary"
             fullWidth
            required
            error={emailErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setPhone(e.target.value)}
            label="Phone"
            variant="outlined"
            color="secondary"
             fullWidth
            required
            error={phoneErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setAge(e.target.value)}
            label="Age"
            variant="outlined"
            color="secondary"
             fullWidth
            required
            error={ageErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>

        <Grid item xs={12}>
            <p style={{ fontSize: "20px" ,textAlign:"left"}}>Gender</p>
            <RadioGroup
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            >
              <FormControlLabel
                value="male"
                control={<Radio />}
                label="Male"
              />
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
              />
               <FormControlLabel
                value="other"
                control={<Radio />}
                label="Other"
              />
            </RadioGroup>
        </Grid>
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setPassword(e.target.value)}
            label="Password"
            variant="outlined"
            color="secondary"
             fullWidth
            required
            error={passwordErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            onChange={(e) => setAddress(e.target.value)}
            label="Address"
            variant="outlined"
            color="secondary"
            multiline
            rows={4}
            fullWidth
            required
            error={addressErr}
            id="margin-dense"
            margin="dense"
          />
        </Grid>
        <Grid item xs={12}>
          <Button
           
            style={{ marginTop: "0%" }}
            onClick={() => console.log("you clicked me")}
            type="submit"
            variant="outlined"
            color="secondary"
            variant="contained"
            endIcon={<KeyboardArrowRightIcon />}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  </Container>
  )
}

export default Register;
