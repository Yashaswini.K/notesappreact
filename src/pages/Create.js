import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import AcUnitOutlinedIcon from "@mui/icons-material/AcUnitOutlined";
import SendIcon from "@mui/icons-material/Send";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import { makeStyles } from "@material-ui/core";
import TextField from "@mui/material/TextField";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import Grid from "@mui/material/Grid";
import { useNavigate } from "react-router-dom";

const useStyle = makeStyles({
  field: {
    marginTop: 20,
    marginBottom: 20,
    display: "block",
  },
  btn: {
    fontSize: 10,
    marginTop: 10,
    backgroundColor: "violet",
    "&:hover": {
      backgroundColor: "blue",
    },
  },
});
function Create() {
  const classes = useStyle();
  //   const history = useHistory();
  const navigate = useNavigate();
  const [title, setTitle] = useState("");
  const [details, setDetails] = useState("");
  const [titleErr, setTitleErr] = useState(false);
  const [detailsErr, setDetailsErr] = useState(false);
  const [category, setCategory] = useState("todos");
  const handleSubmit = (e) => {
    setTitleErr(false);
    setDetailsErr(false);
    if (title === "") {
      setTitleErr(true);
    }
    if (details === "") {
      setDetailsErr(true);
    }
    e.preventDefault();
    if (title && details) {
      //   console.log(title, details, category);
      fetch("http://localhost:8000/notes", {
        method: "POST",
        headers: { "Content-type": "application/json" },
        body: JSON.stringify({ title, details, category }),
      }).then(() => navigate("/"));
    }
  };
  return (
    <Container>
      <Typography
        noWrap
        color="secondary"
        varient="h1"
        gutterBottom
        style={{ fontSize: "25px" }}
      >
        <b> Create Note</b>
      </Typography>
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              onChange={(e) => setTitle(e.target.value)}
              className={classes.field}
              label="Note Title"
              variant="outlined"
              color="secondary"
              fullWidth
              required
              error={titleErr}
              id="margin-dense"
              margin="dense"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              onChange={(e) => setDetails(e.target.value)}
              className={classes.field}
              label="Details"
              variant="outlined"
              color="secondary"
              multiline
              rows={4}
              fullWidth
              required
              error={detailsErr}
              id="margin-dense"
              margin="dense"
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl>
              <FormLabel style={{ fontSize: "20px" }}>Note Category</FormLabel>
              <RadioGroup
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <FormControlLabel
                  value="money"
                  control={<Radio />}
                  label="Money"
                />
                <FormControlLabel
                  value="todos"
                  control={<Radio />}
                  label="Todos"
                />
                <FormControlLabel
                  value="remainders"
                  control={<Radio />}
                  label="Remainders"
                />
                <FormControlLabel
                  value="work"
                  control={<Radio />}
                  label="Work"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.btn}
              style={{ marginTop: "0%" }}
              onClick={() => console.log("you clicked me")}
              type="submit"
              variant="outlined"
              color="secondary"
              variant="contained"
              endIcon={<KeyboardArrowRightIcon />}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
}

export default Create;
