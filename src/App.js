import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Notes from "./pages/Notes";
import Create from "./pages/Create";
import Layout from "./components/Layout";
import { createTheme, ThemeProvider, styled } from "@mui/material/styles";
import Register from "./pages/Register";

function App() {
  return (
    <div>
      {/* <Register/> */}

      <Router>
        <Layout>
          <Routes>

            <Route exact path="/" element={<Notes />} />
            <Route exact path="/create" element={<Create />} />
          </Routes>
        </Layout>
      </Router>
    </div>
  );
}

export default App;
